<?php

namespace Frontend\PostBundle\Controller;

use Backend\UserBundle\Entity\User;
use Frontend\CategoryBundle\Entity\Category;
use Frontend\PostBundle\Entity\Post;
use Frontend\PostBundle\Form\PostFormType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class PostController extends Controller
{
    protected $perPage = 5;
    /**
     * @Route("/", name="home")
     */
    public function indexAction()
    {
        $postsRepo = $this->getDoctrine()->getRepository(Post::class);
        $posts = $postsRepo->findBy([],['published'=>'DESC']);
        $paginator  = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $posts,
            $this->get('request')->query->get('page', 1),
            $this->perPage
        );
        return $this->render('@FrontendPost/Post/index.html.twig',[
            'posts' => $pagination,
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route(path="/post/{id}", methods={"GET"}, name="post_path")
     */
    public function showAction($id)
    {
        $post = $this->getDoctrine()->getRepository(Post::class)->find($id);
        if(!$post){
            throw new NotFoundHttpException("Post not found");
        }
        return $this->render('@FrontendPost/Post/show.html.twig',[
            'post' => $post,
            'user' => $this->getUser(),
            'comments' => $post->getComments(),
        ]);
    }

    /**
     * @Route(path="/manage/post/new", methods={"GET"}, name="new_post_path")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function newAction()
    {
        $form = $this->createForm(new PostFormType());
        return $this->render(
            '@FrontendPost/Post/form.html.twig',
            [
                'form' => $form->createView(),
                'action' => 'create_post_path',
                'messages' => false,
            ]
        );
    }

    /**
     * @Secure(roles={"ROLE_ADMIN", "ROLE_AUTHOR"})
     * @Route(path="/manage/post/create", methods={"POST"}, name="create_post_path")
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm(new PostFormType(), new Post());
        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var Post $newPost */
            $newPost = $form->getData();
            $newPost->setAuthor($this->getUser());
            $newPost->setPublished(new \DateTime());
            $this->getDoctrine()->getManager()->persist($newPost);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirect($this->generateUrl('home'), 301);
        }else{
            $messages = [];
            foreach($form->getErrors() as $error)
            {
                $messages = $error->getMessage();
            }
            return $this->render(
                '@FrontendPost/Post/form.html.twig',
                [
                    'form' => $form->createView(),
                    'action' => 'create_post_path',
                    'messages' => $messages,
                ]
            );
        }
    }

    /**
     * @Secure(roles={"ROLE_ADMIN", "ROLE_AUTHOR"})
     * @Route(path="/manage/post/{id}/edit", methods={"Get"}, name="edit_post_path")
     */
    public function editAction($id)
    {
        /**@var Post $post */
        $post = $this->getDoctrine()->getRepository(Post::class)->find($id);
        /**@var User $user*/
        $user = $this->getUser();
        if($post->getAuthor() == $user)
        {
            $form = $this->createForm(new PostFormType(), $post);
            return $this->render(
                '@FrontendPost/Post/form.html.twig',
                [
                    'form' => $form->createView(),
                    'action' => 'update_post_path',
                    'id' => $id,
                    'messages' => false,
                ]
            );
        }
        else throw new AccessDeniedException("You haven't right to edit this post!");
    }

    /**
     * @Secure(roles={"ROLE_ADMIN", "ROLE_AUTHOR"})
     * @Route(path="/manage/post/{id}", methods={"Post"}, name="update_post_path")
     */
    public function updateAction(Request $request, $id)
    {
        $user = $this->getUser();
        $post = $this->getDoctrine()->getRepository(Post::class)->find($id);

        $form = $this->createForm(new PostFormType(), $post);
        $form->handleRequest($request);

        if($post->getAuthor() == $user){
            if ($form->isValid()) {
                /** @var Post $newPost */
                $post = $form->getData();
                $post->setPublished(new \DateTime());
                $this->getDoctrine()->getManager()->flush();
                return $this->redirect($this->generateUrl('post_path', ['id' => $id]), 301);
            }
            else
            {
                $messages = [];
                foreach($form->getErrors() as $error)
                {
                    $messages = $error->getMessage();
                }
                return $this->render(
                    '@FrontendPost/Post/form.html.twig',
                    [
                        'form' => $form->createView(),
                        'action' => 'update_post_path',
                        'id' => $id,
                        'messages' => $messages,
                    ]
                );
            }
        } throw new AccessDeniedException("You haven't right to edit this post!");
    }

    /**
     * @Secure(roles={"ROLE_ADMIN", "ROLE_AUTHOR"})
     * @Route(path="/manage/post/{id}/delete", methods={"Get"}, name="delete_post_path")
     */
    public function deleteAction($id)
    {
        /** EntityManager @var $em*/
        $em = $this->getDoctrine()->getManager();
        $post = $this->getDoctrine()->getRepository(Post::class)->find($id);
        $em->remove($post);
        $em->flush();
        return $this->redirect($this->generateUrl('home'));
    }

    /**
     * @Route(name="view_by_category", path="/category/{id}", methods={"GET"})
     */
    public function viewByCat($id)
    {
        $postRepo = $this->getDoctrine()->getRepository(Post::class);
        $category = $this->getDoctrine()->getRepository(Category::class)->find($id);
        if(!$category){
            throw new NotFoundHttpException("Category not found");
        }
        $posts = $postRepo->findBy([
            'category' => $category,
        ], [
            'published' => 'DESC',
        ]);

        $paginator  = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $posts,
            $this->get('request')->query->get('page', 1),
            $this->perPage
        );

        return $this->render('@FrontendPost/Post/index.html.twig',
            [
                'posts' => $pagination,
                'user' => $this->getUser(),
            ]);
    }

    /**
     * @Route(name="view_by_author", path="/author/{id}", methods={"GET"})
     */
    public function viewByAuthorAction($id)
    {
        $postRepo = $this->getDoctrine()->getRepository(Post::class);
        $author = $this->getDoctrine()->getRepository(User::class)->find($id);
        if(!$author){
            throw new NotFoundHttpException("Author not found");
        }
        $posts = $postRepo->findBy([
            'author' => $author,
        ], [
            'published' => 'DESC',
        ]);

        $paginator  = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $posts,
            $this->get('request')->query->get('page', 1),
            $this->perPage
        );

        return $this->render('@FrontendPost/Post/index.html.twig',
            [
                'posts' => $pagination,
                'user' => $this->getUser(),
            ]);
    }
}
