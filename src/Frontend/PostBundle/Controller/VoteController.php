<?php

namespace Frontend\PostBundle\Controller;

use Backend\UserBundle\Entity\User;
use Frontend\CategoryBundle\Entity\Category;
use Frontend\PostBundle\Entity\Post;
use Frontend\PostBundle\Entity\PostRate;
use Frontend\PostBundle\Form\PostFormType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class VoteController extends Controller
{
    /**
     * @Secure(roles={"ROLE_ADMIN", "ROLE_COMMENTER", "ROLE_AUTHOR"})
     * @Route(name="post_vote_up", path="/post/{id}/voteUp", methods={"GET"})
     */
    public function voteUpAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $post = $this->getDoctrine()->getRepository(Post::class)->find($id);
        if(!$post){
            throw new NotFoundHttpException('Post not found!');
        }
        if(!$this->getUser() or $this->getUser() == $post->getAuthor()){
            throw new AccessDeniedException("You can't vote!");
        }

        /** @var PostRate $hasVote */
        $hasVote = $this->getDoctrine()->getRepository(PostRate::class)->findOneBy([
            'post' => $post,
            'user' => $this->getUser()
        ]);
        if($hasVote){
            if($hasVote->getVote() == '-1'){
                $hasVote->setVote('1');
                $em->flush();

                $response = new Response(json_encode(array('vote' => $post->showRate())));
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            } else {
                $response = new Response(json_encode(array('vote' => $post->showRate())));
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }
        } else {
            $newVote = new PostRate();
            $newVote->setVote('+1');
            $newVote->setUser($this->getUser());
            $newVote->setPost($post);
            $em->persist($newVote);
            $em->flush();

            $response = new Response(json_encode(array('vote' => $post->showRate())));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }

    /**
     * @Secure(roles={"ROLE_ADMIN", "ROLE_COMMENTER", "ROLE_AUTHOR"})
     * @Route(name="post_vote_down", path="/post/{id}/voteDown", methods={"GET"})
     */
    public function voteDownAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $post = $this->getDoctrine()->getRepository(Post::class)->find($id);
        if(!$this->getUser() or $this->getUser() == $post->getAuthor()){
            throw new AccessDeniedException("You can't vote!");
        }
        if(!$post){
            throw new NotFoundHttpException('Post not found!');
        }
        /** @var PostRate $hasVote */
        $hasVote = $this->getDoctrine()->getRepository(PostRate::class)->findOneBy([
            'post' => $post,
            'user' => $this->getUser()
        ]);
        if($hasVote){
            if($hasVote->getVote() == '+1'){
                $hasVote->setVote('-1');
                $em->flush();

                $response = new Response(json_encode(array('vote' => $post->showRate())));
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            } else {
                $response = new Response(json_encode(array('vote' => $post->showRate())));
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }
        } else {
            $newVote = new PostRate();
            $newVote->setVote('-1');
            $newVote->setUser($this->getUser());
            $newVote->setPost($post);
            $em->persist($newVote);
            $em->flush();

            $response = new Response(json_encode(array('vote' => $post->showRate())));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }

    /**
     * @param $id
     * @Route(name="post_show_vote", path="/post/{id}/vote", methods={"POST"})
     */
    public function showVoteAction($id)
    {

    }
}
