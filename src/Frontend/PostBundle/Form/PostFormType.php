<?php
/**
 * Created by PhpStorm.
 * User: owner
 * Date: 8/20/14
 * Time: 1:15 PM
 */

namespace Frontend\PostBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class PostFormType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', 'text');
        $builder->add('text', 'textarea');
        $builder->add('category', 'entity', [
                'class' => 'Frontend\CategoryBundle\Entity\Category',
                'property' => 'title'
            ]);
    }
    public function getName()
    {
        return 'post_form';
    }
} 