<?php

namespace Frontend\PostBundle\Twig;

class Truncation extends \Twig_Extension{
    public function getName()
    {
        return 'truncation';
    }

    public function getFilters()
    {
        return array(
            'truncate' => new \Twig_Filter_Method($this, 'truncate')
        );
    }

    public function truncate($text, $max = 30)
    {
        $lastSpace = 0;

        if (strlen($text) >= $max)
        {
            $text = substr($text, 0, $max);
            $lastSpace = strrpos($text,' ');
            $text = substr($text, 0, $lastSpace).'...';
        }

        return $text;
    }
} 