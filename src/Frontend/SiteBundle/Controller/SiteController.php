<?php

namespace Frontend\SiteBundle\Controller;

use Frontend\PostBundle\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class SiteController extends Controller
{
    /**
     * @Route(path="/sidebar", methods={"get"}, name="get_side_bar")
     */
    public function getSideBarAction()
    {
        return $this->render(
            '@FrontendSite/Site/sideBar.html.twig',
            [
                'articles' => $this->getDoctrine()->getRepository(Post::class)->findBy([],['id' => 'DESC'],5),
                'user' => $this->getUser(),
            ]
        );
    }
}
