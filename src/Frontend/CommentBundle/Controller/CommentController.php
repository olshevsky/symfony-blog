<?php

namespace Frontend\CommentBundle\Controller;

use Backend\UserBundle\Entity\User;
use Frontend\CommentBundle\Entity\Comment;
use Frontend\CommentBundle\Form\CommentFormType;
use Frontend\PostBundle\Entity\Post;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class CommentController extends Controller
{
    /**
     * @Route(name="post_comment_form", path="/post/{id}/comment_form", methods={"GET"})
     */
    public function getCommentFormAction($id)
    {
        if($this->getUser()){
            $user =$this->getUser();
        }
        else{
            $user = false;
        }
        $form = $this->createForm(new CommentFormType());
        return $this->render(
            '@FrontendComment/Comment/_comment_form.html.twig',
            [
                'form' => $form->createView(),
                'post_id' => $id,
                'user' => $user,
            ]
        );
    }

    /**
     * @Route(name="post_save_comment_path", path="/post/{id}/comments", methods={"POST"})
     */
    public function saveCommentAction($id, Request $request)
    {
        $form = $this->createForm(new CommentFormType(), new Comment());
        $form->handleRequest($request);
        $parent = $request->get('comment_form')['parent'];
        if($parent){
            $parentComment = $this->getDoctrine()->getRepository(Comment::class)->find($parent);
        } else {
            $parentComment = null;
        }
        $post = $this->getDoctrine()->getRepository(Post::class)->find($id);
        if(!$post){
            throw new NotFoundHttpException("Post not found");
        }

        if ($form->isValid()) {
            /** @var Comment $newComment */
            $newComment = $form->getData();
            $newComment->setAuthor($this->getUser());
            $newComment->setPublished(new \DateTime());
            $newComment->setPost($post);
            $newComment->setParent($parentComment);
            $this->getDoctrine()->getManager()->persist($newComment);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirect($this->generateUrl('post_path',['id'=>$id]), 301);
        }
    }

    /**
     * @Secure(roles={"ROLE_ADMIN", "ROLE_AUTHOR"})
     * @Route(name="delete_comment_path", path="/comment/{id}/delete", methods={"GET"})
     */
    public function deleteCommentAction($id)
    {
        $comment = $this->getDoctrine()->getRepository(Comment::class)->find($id);
        /**@var Post $post*/
        $post = $comment->getPost();

        $em = $this->getDoctrine()->getManager();

        /**@var User $user*/
        $user = $this->getUser();
        if($user->getRole() == User::ADMIN_ROLE or $post->getAuthor() == $user)
        {
            $em->remove($comment);
            $em->flush();
            return $this->redirect($this->generateUrl('post_path',['id'=>$post->getId()]), 301);
        }
        else{
            throw new AccessDeniedException("Access denied. You can't remove this comment!");
        }
    }
}
