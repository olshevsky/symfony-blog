<?php
/**
 * Created by PhpStorm.
 * User: owner
 * Date: 8/20/14
 * Time: 1:15 PM
 */

namespace Frontend\CommentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CommentFormType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('text', 'textarea');
        $builder->add('parent', 'hidden');
    }
    public function getName()
    {
        return 'comment_form';
    }
} 