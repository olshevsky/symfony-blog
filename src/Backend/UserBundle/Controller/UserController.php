<?php

namespace Backend\UserBundle\Controller;

use Backend\UserBundle\Form\ChangePasswordFormType;
use Backend\UserBundle\Form\UpdateFormType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use DateTime;
use Backend\UserBundle\Form\RegistrationFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\SecurityContext;

use Backend\UserBundle\Entity\User;

class UserController extends Controller
{
    /**
     * @Route(name="security_login", methods={"GET"}, path="/login")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function loginAction()
    {
        if ($this->get('request')->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $this->get('request')->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $this->get('request')->getSession()->get(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render('@BackendUser/User/login.html.twig', array(
            'last_username' => $this->get('request')->getSession()->get(SecurityContext::LAST_USERNAME),
            'error' => $error
        ));
    }
    /**
     * $Route(name="security_logout", path="/logout")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function logout()
    {

    }

    /**
     * @Route("/admin", name="admin_path", methods={"GET"})
     */
    public function adminAction()
    {
        return $this->render('@BackendUser/User/admin.html.twig');
    }

    /**
     * @Route(path="/registration", name="reg_path", methods={"GET"})
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Template()
     */
    public function regAction()
    {
        $form = $this->createForm(new RegistrationFormType());
        return $this->render(
            '@BackendUser/User/reg.html.twig',
            [
                'form' => $form->createView(),
                'messages' => null
            ]
        );
    }

    /**
     * @Route(path="/registration", name="new_user", methods={"POST"})
     */
    public function regUserAction(Request $request)
    {
        $form = $this->createForm(new RegistrationFormType(), new User());
        $form->handleRequest($request);
        $factory = $this->get('security.encoder_factory');
        if ($form->isValid()) {
            /** @var User $newUser */
            $newUser = $form->getData();

            $newUser->setSalt(rand(1,100));

            $encoder = $factory->getEncoder($newUser);
            $password = $encoder->encodePassword($newUser->getPassword(), $newUser->getSalt());
            $newUser->setPassword($password);

            $this->getDoctrine()->getManager()->persist($newUser);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirect($this->generateUrl('home'), 301);
        }
        else{
            if($form->getErrors(true, true)){
                $messages = [];
                foreach($form->getErrors(true, true) as $error){
                    $messages[] = $error->getMessage();
                }
                return $this->render('@BackendUser/User/reg.html.twig', array(
                    'form' => $form->createView(),
                    'messages' => $messages,
                ));
            }
        }
    }

    /**
     * @Secure(roles={"ROLE_ADMIN", "ROLE_AUTHOR", "ROLE_USER", "ROLE_COMMENTER"})
     * @Route(name="account_manage_path", path="/manage/account", methods={"GET"})
     */
    public function accountAction()
    {
        $user_form = $this->createForm(new UpdateFormType(), $this->getUser());
        $passwordForm = $this->createForm(new ChangePasswordFormType(), $this->getUser());
        return $this->render('@BackendUser/User/manage.html.twig', [
            'user_form' => $user_form->createView(),
            'password_form' => $passwordForm->createView(),
            'user_messages' => false,
            'password_messages' => false,
            'user' => $this->getUser(),
        ]);
    }

    /**
     * @Secure(roles={"ROLE_ADMIN", "ROLE_AUTHOR", "ROLE_USER", "ROLE_COMMENTER"})
     * @Route(name="update_user", path="/manage/account", methods={"POST"})
     */
    public function updateAccountAction(Request $request)
    {
        $admin = false;
        if($this->getUser()->getRole() == User::ADMIN_ROLE)
            $admin = true;

        $passwordForm = $this->createForm(new ChangePasswordFormType(), $this->getUser());
        $user_form = $this->createForm(new UpdateFormType(), $this->getUser());
        $user_form->handleRequest($request);

        $factory = $this->get('security.encoder_factory');
        if ($user_form->isValid()) {
            /** @var User $newUser */
            $user = $this->getDoctrine()->getRepository(User::class)->find($this->getUser()->getId());
            if($admin){
                $user->setRole(User::ADMIN_ROLE);
            }
            $this->getDoctrine()->getManager()->flush();
            return $this->redirect($this->generateUrl('account_manage_path'), 301);
        }
        else{
            if($user_form->getErrors(true, true)){
                $messages = [];
                foreach($user_form->getErrors(true, true) as $error){
                    $messages[] = $error->getMessage();
                }
                return $this->render('@BackendUser/User/manage.html.twig', array(
                    'user_form' => $user_form->createView(),
                    'user_messages' => $messages,
                    'password_form' => $passwordForm->createView(),
                    'password_messages' => false,
                    'user' => $this->getUser(),
                ));
            }
        }
    }

    /**
     * @Secure(roles={"ROLE_ADMIN", "ROLE_AUTHOR", "ROLE_USER", "ROLE_COMMENTER"})
     * @Route(name="change_user_password", path="/manage/account/change_password", methods={"POST"})
     */
    public function changePasswordAction(Request $request)
    {
        $passwordForm = $this->createForm(new ChangePasswordFormType(), $this->getUser());
        $user_form = $this->createForm(new UpdateFormType(), $this->getUser());

        $passwordForm->handleRequest($request);
        $factory = $this->get('security.encoder_factory');
        if ($passwordForm->isValid()) {
            $updUser = $this->getDoctrine()->getRepository(User::class)->find($this->getUser()->getId());
            $updUser->setSalt(rand(1,100));

            $encoder = $factory->getEncoder($updUser);
            $password = $encoder->encodePassword($updUser->getPassword(), $updUser->getSalt());
            $updUser->setPassword($password);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirect($this->generateUrl('account_manage_path'), 301);
        }
        else{
            if($passwordForm->getErrors(true, true)){
                $messages = [];
                foreach($passwordForm->getErrors(true, true) as $error){
                    $messages[] = $error->getMessage();
                }
                return $this->render('@BackendUser/User/manage.html.twig', array(
                    'password_form' => $passwordForm->createView(),
                    'password_messages' => $messages,
                    'user_form' => $user_form->createView(),
                    'user_messages' => false,
                    'user' => $this->getUser(),
                ));
            }
        }
    }
}
