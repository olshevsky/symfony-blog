<?php
/**
 * Created by PhpStorm.
 * User: owner
 * Date: 8/20/14
 * Time: 3:41 PM
 */

namespace Backend\UserBundle\Form;


use Backend\UserBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ChangePasswordFormType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('password', 'repeated', array(
            'type' => 'password',
            'invalid_message' => 'The password fields must match.',
            'options' => array('attr' => array('class' => 'password-field')),
            'required' => true,
            'first_options'  => array('label' => 'Password'),
            'second_options' => array('label' => 'Repeat Password'),
        ));
    }
    public function getName()
    {
        return('update_user_form');
    }
} 