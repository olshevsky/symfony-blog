<?php
/**
 * Created by PhpStorm.
 * User: owner
 * Date: 8/20/14
 * Time: 3:41 PM
 */

namespace Backend\UserBundle\Form;


use Backend\UserBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationFormType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName', 'text');
        $builder->add('lastName', 'text');
        $builder->add('username', 'text');
        $builder->add('email', 'email');
        $builder->add('password', 'password');
        $builder->add('role', 'choice', array(
        'choices' => [
            User::AUTHOR_ROLE => 'Author',
            User::COMMENTER_ROLE => 'Commenter'
        ]));
    }
    public function getName()
    {
        return('registration');
    }
} 