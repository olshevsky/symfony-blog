<?php

namespace Backend\ManageBundle\Controller;

use Backend\UserBundle\Entity\User;
use Frontend\CommentBundle\Entity\Comment;
use Frontend\CommentBundle\Form\CommentFormType;
use Frontend\PostBundle\Entity\Post;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


class ManageController extends Controller
{
    /**
     * @Secure(roles={"ROLE_ADMIN", "ROLE_AUTHOR"})
     * @Route(name="posts_manage_path", path="/manage/posts", methods={"GET"})
     */
    public function postsAction()
    {
        $posts = $this->getDoctrine()->getRepository(Post::class)->findBy([
                'author' => $this->getUser()
        ]);
        return $this->render('@BackendManage/Manage/post.html.twig', [
            'user' => $this->getUser(),
            'posts' => $posts
        ]);
    }

    /**
     * @Secure(roles={"ROLE_ADMIN"})
     * @Route(name="comments_manage_path", path="/manage/comments", methods={"GET"})
     */
    public function commentsAction()
    {
        return $this->render('BackendManageBundle:Manage:index.html.twig');
    }
}
