$('p.showReply').hide();

$('a.voteUp').on('click', function(){
    id = this.getAttribute('post');
    vote_up(id);
    return false;
});
$('a.voteDown').on('click', function(){
    id = this.getAttribute('post');
    vote_down(id);
    return false;
});

function vote_up(id){
    var request = $.ajax({
        url: "/post/"+id+"/voteUp",
        type: "GET",
        dataType: "json"
    });

    request.done(function( msg ) {
        var block = $('.postMarks[post="'+id+'"]').find('span.status');
        var vote = 0;
        if(msg.vote > 0){
            block.removeClass('voteMinus');
            block.addClass('votePlus');
            vote = "+" + msg.vote.toString();
        } else if(msg.vote < 0){
            block.removeClass('votePlus');
            block.addClass('voteMinus');
            vote = msg.vote.toString();
        } else{
            vote = "&mdash;";
        }
        block.empty().text(vote);
        console.log(vote);
    });

    request.fail(function( jqXHR, textStatus ) {
        alert("You already vote!");
    });
}
function vote_down(id){
    var request = $.ajax({
        url: "/post/"+id+"/voteDown",
        type: "GET",
        dataType: "json"
    });

    request.done(function( msg ) {
        var block = $('.postMarks[post="'+id+'"]').find('span.status');
        var vote = 0;
        if(msg.vote > 0){
            block.removeClass('voteMinus');
            block.addClass('votePlus');
            vote = "+" + msg.vote.toString();
        } else if(msg.vote < 0){
            block.removeClass('votePlus');
            block.addClass('voteMinus');
            vote = msg.vote.toString();
        } else{
            vote = "&mdash;";
        }
        block.empty().html(vote);
        console.log(vote);
    });

    request.fail(function( jqXHR, textStatus ) {
        alert("You already vote!");
    });
}

$('a.reply-btn').on('click', function(){
    var replyTo = this.getAttribute('author');
    var authorName = $(this).parent().parent().find('b.commentAuthor').find('span').text();
    console.log(authorName);
    var dispalyName = $('p.showReply');
    dispalyName.show(500);
    dispalyName.find('span.comment_id').html(authorName);
    $('#comment_form_parent').val(replyTo);
    return false;
});

$('a.removeAuthor').on('click', function(){
    $('#comment_form_parent').val(null);
    var dispalyName = $('p.showReply');
    dispalyName.hide(500);
    return false;
});